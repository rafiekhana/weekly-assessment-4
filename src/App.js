import "./App.css";

function App() {
  return (
    <div className="App">
      <div class="page">
        <div class="subject">
          <div class="subject__title">
            <h1>GOT MARKETING? ADVANCE YOUR BUSINESS INSIGHT.</h1>
            <p>Fill out the form and receive our award winning newsletter.</p>
          </div>
        </div>
        <div class="signin">
          <div class="signin__form">
            <div class="signin__inputName">
              <div class="details">Name</div>
              <input type="text" class="text" />
            </div>
            <div class="signin__inputEmail">
              <div class="details">Email</div>
              <input type="email" class="text" />
            </div>
            {/* <input class="btn" type="submit" value="Sign me up" /> */}
            <button>Sign me up</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
